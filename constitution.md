# Some simple rules we'll all follow


1. In order to have access to resources, the project must be under the HelixSoftware organisation
 (We need to be able to track the projects that HelixSoftware is devoting resources to, and be able to give help to each other.)
 
2. Each project shares any resources it can spare.

3. Staff aren't project specific, staff are part of the organisation
 (The organization can allocate staff to any particular project or projects that it wants, according to their roles and strengths.  For example, someone could be assigned solely as tech support to Helix and the Rule34 downloader; however, this is up to the Directorate, support manager, and staff, not any individual staff or project.)

4. All projects are the creative property of you, however for legality, HelixSoftware will have custody over them
 (What this means is that if you abandon your project, HelixSoftware can take it and give it to someone who wants to maintain it.  If this happens, you will not necessarily get the creative direction of your project back.  You can always take the code as it was when you abandoned it and start work on it somewhere else, though.)

5. All projects must abide the rules for that project type

6. Helix Software reserves the right to remove any project from the organisation without notice

7. There are different things we're talking about here, and so we're going to define them:

  - "Staff" means "any individual who has agreed to dedicate some portion of their time to promoting HelixSoftware's purposes".  A developer is staff.  A tech support representative is staff.  An artist is staff.  A moderator is staff.  A Director is staff.  A project owner is staff.  Nobody can be forced to devote more time to HelixSoftware than they can afford.  But by the same token, people who don't devote enough time to HelixSoftware can be removed from staff if the projects they're involved with don't want them.

  - "Project" means "a project owner and some number of other staff who create a piece of software for public service or usage that performs at least one unique function."

  - "Directorate" means "A specially-designated group within HelixSoftware which is responsible for accepting projects to receive HelixSoftware resources and evaluating projects on an ongoing basis to determine if they should still receive HelixSoftware support."

  - "Resources" means any staff, service, hardware, software, or other resources that have been committed to a project.  At least at first, this also includes resources committed by the owner and staff of a project, not just resources committed by HelixSoftware itself.

