# Here are the rules Discord bots must follow to be supported

1. **Commands should be explicitly invoked**. Bots should not activate on
normal chat. Instead, use a command prefix or only respond when your bot is
directly @mentioned.

2. **Use unique prefixes**. Single-character prefixes such as `!`, `$` and `.`
are commonplace for activating commands and lead to overlaps with other bots, espescially with othoer bots in the organisation. As such avoid using prefixes that are common place or already in use by another bot within HelixSoftware

3. **Don't reply with "invalid command"**. If a user uses a command that does
not exist, then let it fail silently. Do not have it reply with something like
"invalid command". Though if the command is correct, but arguments are wrong
then it's okay to reply with "invalid args".

4. **Be respectful of Discord's API**. Bots that abuse and misuse the Discord
API ruin things for everyone. Make sure to factor in rate-limiting and backoff
in your bot code, and be intelligent about using the API.

5. **Ignore both your own and other bots' messages**. This helps prevent infinite
self-loops and potential security exploits. Using a zero width space such as `\u200B`
and `\u180E` in the beginning of each message also prevents your bot from
triggering other bots' commands.

6. **You must keep your bot updated** An out of date bot is just a buggy mess that nobody will use, and is a waste of our resources

7. **Have an info command**. The info command must have your api name, server count, and state it is part of HelixSoftware
